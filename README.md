# FaceMerge

#### 介绍
本项目最终实现一个Java版本的人脸替换软件
1. 本项目javacv技术无需手动安装opencv
2. 虽然技术手段不同，但是核心算法还是一样的 
3. 使用opencv的68个关键点实行人脸融合
4. 使用javacv结合百度AI人脸检测(需要人脸72个关键点)实现人脸融合
5. 只要会用java就可以轻松实现人脸融合，FaceDetect中有两种关键点识别方法，一个是基于百度API,一个是基于opencv的。

1、使用opencv识别关键点效果图
![图片融合效果](https://gitee.com/endlesshh/FaceMerge/raw/master/img/1FACE.jpg)
![图片融合效果](https://gitee.com/endlesshh/FaceMerge/raw/master/img/2FACE.jpg)
![图片融合效果](https://gitee.com/endlesshh/FaceMerge/raw/master/img/3SANJIAO1.jpg)
![图片融合效果](https://gitee.com/endlesshh/FaceMerge/raw/master/img/4SANJIAO1.jpg)
![图片融合效果](https://gitee.com/endlesshh/FaceMerge/raw/master/img/result.jpg)


 Required Software
------------
本版本在以下平台测试通过：
* windows7 64bit
* jdk1.8.0_45
* junit 4
* opencv4.3
* javaccp1.5.3

#### 借鉴项目和博客
1. https://github.com/lichao3140/Opencv_Java
2. https://gitee.com/xshuai/FaceMerge
3. https://blog.csdn.net/kangqi5602/article/details/78132240?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase
#### 下一步计划
1. 使用javacv将视频进行音频和图片分离，合并。(已完成)
2. 使用虹软sdk进行人脸识别获取人脸图片。
3. 利用人脸融合进行图片的人脸替换。
4. 重新合成视频文件。